import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providerbookingdetails',
  templateUrl: './providerbookingdetails.page.html',
  styleUrls: ['./providerbookingdetails.page.scss'],
})
export class ProviderbookingdetailsPage implements OnInit {

  isLoading = false;
  isDisabled: boolean = false;
  userdata: any = [];
  data: any = [];
  bookingDetails: any = '';

  bookingID : any = '';

  constructor(
    private navCtrl: NavController,
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private activatedRoute: ActivatedRoute
  ) { 
    this.activatedRoute.queryParams.subscribe((res) => {
      this.bookingID = res.bookID;
    });
  }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.loginToken();
  }

  getDetails(){
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        booking_detail_id: this.bookingID
      };
      this.authService.postData("get-booking-details", body).then(result => {
        this.data = result;
        console.log("Booking Details: ", this.data);
        this.bookingDetails = this.data.result.data[0];
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onPayOut(){
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        booking_detail_id: this.bookingID
      };
      this.authService.postData("post-payment-request", body).then(result => {
        this.hideLoader();
        this.data = result;
        console.log("Payout: ", this.data);
        if (this.data.status.error_code == 0) {
          this.presentToast(this.data.status.message);
          this.getDetails();
        } else {
          this.presentToast(this.data.status.message);
        } 
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
