import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvidernotificationPage } from './providernotification.page';

const routes: Routes = [
  {
    path: '',
    component: ProvidernotificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidernotificationPageRoutingModule {}
