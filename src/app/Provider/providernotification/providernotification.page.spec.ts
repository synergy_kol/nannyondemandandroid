import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvidernotificationPage } from './providernotification.page';

describe('ProvidernotificationPage', () => {
  let component: ProvidernotificationPage;
  let fixture: ComponentFixture<ProvidernotificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidernotificationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvidernotificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
