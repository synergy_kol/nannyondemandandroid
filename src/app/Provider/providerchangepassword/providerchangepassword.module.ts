import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProviderchangepasswordPageRoutingModule } from './providerchangepassword-routing.module';

import { ProviderchangepasswordPage } from './providerchangepassword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProviderchangepasswordPageRoutingModule
  ],
  declarations: [ProviderchangepasswordPage]
})
export class ProviderchangepasswordPageModule {}
