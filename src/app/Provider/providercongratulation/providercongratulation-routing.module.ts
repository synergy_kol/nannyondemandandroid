import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvidercongratulationPage } from './providercongratulation.page';

const routes: Routes = [
  {
    path: '',
    component: ProvidercongratulationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidercongratulationPageRoutingModule {}
