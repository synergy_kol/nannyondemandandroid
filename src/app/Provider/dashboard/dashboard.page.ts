import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, MenuController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  isLoading = false;
  userdata: any = [];
  data: any = [];
  profileData: any = [];
  age: any;

  constructor(
    private menuCtrl: MenuController,
    public navCtrl: NavController,
    private alertController: AlertController,
    private storage: Storage,
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.menuCtrl.swipeGesture(false);
    this.showLoader('Please wait...');
    this.getDetails(); 
    this.loadPage(); 
    this.loginToken(); 
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-profile", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.profileData = this.data.result.data;
        // if(this.profileData.payment_method == ''){
        //   this.navCtrl.navigateRoot('/payment-connect');
        // }
        this.hideLoader();
        this.age =  this.calculateAge(this.age);
      },
        error => {
          this.hideLoader();
      });
    });
  }

  loadPage() {
    setTimeout(() => {
      this.getDetails();
      this.loadPage();
    }, 1000)
  }

  calculateAge(dob) {
    this.age = dob;
    var dob_entry = this.profileData.dob;
    var today = new Date();
    var birthDate = new Date(dob_entry);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }

  goProfile() {
    this.navCtrl.navigateForward('/providerprofile');
  }

  goBooking() {
    this.navCtrl.navigateForward('/providerbookings');
  }

  goEarning() {
    this.navCtrl.navigateForward('/earnings');
  }

  goNoti() {
    this.navCtrl.navigateForward('/providernotification');
  }

  goHelp() {
    this.navCtrl.navigateForward('/providerhelp');
  }
  goTC() {
    this.navCtrl.navigateForward('/providertermasconditions');
  }

  goChangepass(){
    this.navCtrl.navigateForward('/providerchangepassword');
  }

  goChangepayment(){
    this.navCtrl.navigateRoot('/payment-connect');
  }

  onLogout() {
    const alert = this.alertController.create({
      header: 'Confirm Logout!',
      message: 'Are you sure, you want to Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
