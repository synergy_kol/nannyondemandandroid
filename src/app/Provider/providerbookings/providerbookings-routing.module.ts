import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderbookingsPage } from './providerbookings.page';

const routes: Routes = [
  {
    path: '',
    component: ProviderbookingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProviderbookingsPageRoutingModule {}
