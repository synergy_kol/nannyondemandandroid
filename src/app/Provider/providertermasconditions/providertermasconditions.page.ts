import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providertermasconditions',
  templateUrl: './providertermasconditions.page.html',
  styleUrls: ['./providertermasconditions.page.scss'],
})
export class ProvidertermasconditionsPage implements OnInit {

  isLoading = false;
  data: any = [];
  pageData: any = '';
  userdata: any = [];

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
    private navCtrl: NavController
  ) { }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.loginToken();
  }

  getDetails(){
      var body = {
        page_slug: "terms-and-conditions",
      };
      this.authService.postData("get-cms-content", body).then(result => {
        this.data = result;
        this.pageData = this.data.result.data.description;
        console.log("Content: ", this.data);
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
