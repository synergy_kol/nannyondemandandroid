import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Providerregistration2Page } from './providerregistration2.page';

describe('Providerregistration2Page', () => {
  let component: Providerregistration2Page;
  let fixture: ComponentFixture<Providerregistration2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Providerregistration2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Providerregistration2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
