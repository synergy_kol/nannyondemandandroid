import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProviderprofilePageRoutingModule } from './providerprofile-routing.module';

import { ProviderprofilePage } from './providerprofile.page';
import { StarRatingModule } from 'ionic5-star-rating';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProviderprofilePageRoutingModule,
    StarRatingModule,
    NgxIonicImageViewerModule
  ],
  declarations: [ProviderprofilePage]
})
export class ProviderprofilePageModule {}
