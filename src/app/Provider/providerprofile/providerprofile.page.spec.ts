import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProviderprofilePage } from './providerprofile.page';

describe('ProviderprofilePage', () => {
  let component: ProviderprofilePage;
  let fixture: ComponentFixture<ProviderprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderprofilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProviderprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
