import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
declare var cordova: any;

@Component({
  selector: 'app-providerprofile',
  templateUrl: './providerprofile.page.html',
  styleUrls: ['./providerprofile.page.scss'],
})
export class ProviderprofilePage implements OnInit {

  isLoading = false;
  isDisabled: boolean = false;
  userdata: any = [];
  data: any = [];
  profileData: any = [];
  profilePic: any = [];
  age: any;
  galleryData: any = [];
  allGPimages: any = [];
  GPimage: any = '';
  reviewData: any = [];

  showabout = false;
  abactive = "";
  showgallery = true;
  glactive = "activetab";
  showreview = false;
  rvactive = "";

  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private actionSheet: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    private filePath: FilePath,
    private file: File,
    private toastController: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  about() {
    this.showabout = true;
    this.showgallery = false;
    this.showreview = false;
    this.abactive = "activetab";
    this.glactive = "";
    this.rvactive = "";
  }
  gallery() {
    this.showabout = false;
    this.showgallery = true;
    this.showreview = false;
    this.abactive = "";
    this.glactive = "activetab";
    this.rvactive = "";
  }
  review() {
    this.showabout = false;
    this.showgallery = false;
    this.showreview = true;
    this.abactive = "";
    this.glactive = "";
    this.rvactive = "activetab";
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.getGallery();
    this.loginToken();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-profile", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.profileData = this.data.result.data;
        this.profilePic = this.profileData.profile_image;
        this.reviewData = this.profileData.reviews;
        this.hideLoader();
        this.age = this.calculateAge(this.age);
      },
        error => {
          this.hideLoader();
        });
    });
  }

  calculateAge(dob) {
    this.age = dob;
    var dob_entry = this.profileData.dob;
    var today = new Date();
    var birthDate = new Date(dob_entry);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;  
  }

  getGallery() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-photo-gallery-images", body).then(result => {
        this.data = result;
        console.log("Gallery: ", this.data);
        this.galleryData = this.data.result.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  uploadPP() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takePP(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.profilePic = 'data:image/jpeg;base64,' + imagePath;
      this.saveProfilePic(this.profilePic);
    }, (err) => {
      console.log(err);
    });
  }
  saveProfilePic(pic) {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        image: pic
      }
      this.authService.postData("ProfileImageUpload", body).then(result => {
        this.data = result;
        console.log("Profile Image: ", this.data);
        if (this.data.status.error_code == 0) {
          this.presentToast("Profile Picture Succesfully Uploaded.");
          this.getDetails();
        } else {
          this.presentToast(this.data.status.message);
          this.hideLoader();
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  uploadGP() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takeGP(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takeGP(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takeGP(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,//for ios only
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      let data = {
        image: 'data:image/jpeg;base64,' + imagePath
      }
      this.allGPimages.push(data);
      this.GPimage = this.allGPimages;
      this.saveGalleryPic(this.GPimage);
    }, (err) => {
      console.log(err);
    });
  }
  saveGalleryPic(GPpic) {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        images: GPpic
      }
      this.authService.postData("save-photo-gallery-images", body).then(result => {
        this.data = result;
        console.log("Saved Gallery Image: ", this.data);
        if (this.data.status.error_code == 0) {
          this.presentToast("Image Succesfully Uploaded.");
          this.allGPimages = [];
          this.getGallery();
        } else {
          this.presentToast(this.data.status.message);
          this.hideLoader();
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }
  rmimage(uID, imgID) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Remove?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Delete',
          handler: () => {
            this.showLoader('Removing...');
            setTimeout(() => {
              var body = {
                user_id: uID,
                image_id: imgID
              }
              this.authService.postData("remove-gallery-image", body).then(result => {
                this.data = result;
                console.log("Remove Image: ", this.data);
                if (this.data.status.error_code == 0) {
                  this.presentToast("Image Succesfully Removed.");
                  this.getGallery();
                } else {
                  this.presentToast(this.data.status.message);
                  this.hideLoader();
                }
              },
                error => {
                  this.hideLoader();
                });
            }, 3000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  goEdit() {
    this.navCtrl.navigateForward('/providerregistration1');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
