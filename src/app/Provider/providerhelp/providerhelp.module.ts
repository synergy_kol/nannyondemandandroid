import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProviderhelpPageRoutingModule } from './providerhelp-routing.module';

import { ProviderhelpPage } from './providerhelp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProviderhelpPageRoutingModule
  ],
  declarations: [ProviderhelpPage]
})
export class ProviderhelpPageModule {}
