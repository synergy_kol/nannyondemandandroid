import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProviderhelpPage } from './providerhelp.page';

describe('ProviderhelpPage', () => {
  let component: ProviderhelpPage;
  let fixture: ComponentFixture<ProviderhelpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderhelpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProviderhelpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
