import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providerregistration1',
  templateUrl: './providerregistration1.page.html',
  styleUrls: ['./providerregistration1.page.scss'],
})
export class Providerregistration1Page implements OnInit {

  servicetype: any = '';
  fname: any = '';
  lname: any = '';
  dob: any = '';
  address: any = '';
  city: any = '';
  state: any = '';
  zip: any = '';
  phone: any = '';
  emgnphone: any = '';
  email: any = '';
  password: any = '';
  cnfpassword: any = '';

  isDisabled: boolean = false;
  isLoading = false;

  data: any = [];
  servicetypeData: any = [];

  userdata: any = [];
  profileData: any = [];
  hideextra = true;

  step1Data: any = [];

  customActionSheetOptions: any = {

  };

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider
  ) { }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onNext();
    }
  }

  ngOnInit() {
  }

  getServicedata() {
    var body = {
      source: 'mob',
    };
    this.authService.postData("service-type-list", body).then(val => {
      this.data = val;
      this.servicetypeData = this.data.result.data;
      this.hideLoader();
      console.log("Service Type:", this.servicetypeData);
    },
      error => {
        this.hideLoader();
      });
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.storage.get('userDetails').then((val) => {
      if (val != null) {
        this.hideextra = false;
        this.userdata = val;
        this.getProfileDetails();
      } else {
        this.storage.get('setp1').then((val) => {
          this.step1Data = val;
          this.servicetype = this.step1Data.servicetype;
          this.fname = this.step1Data.fname;
          this.lname = this.step1Data.lname;
          this.dob = this.step1Data.dob;
          this.address = this.step1Data.address;
          this.city = this.step1Data.city;
          this.state = this.step1Data.state;
          this.zip = this.step1Data.zip;
          this.phone = this.step1Data.phone;
          this.emgnphone = this.step1Data.emgnphone;
          this.email = this.step1Data.email;
        });
      }
    });
    this.getServicedata();
    this.loginToken();
  }

  getProfileDetails() {
    var body = {
      user_id: this.userdata.user_id,
    };
    this.authService.postData("get-profile", body).then(result => {
      this.data = result;
      console.log("profile: ", this.data);
      this.profileData = this.data.result.data;
      this.servicetype = this.profileData.service_type_id;
      this.fname = this.profileData.fname;
      this.lname = this.profileData.lname;
      this.dob = this.profileData.dob;
      this.address = this.profileData.address;
      this.city = this.profileData.city;
      this.state = this.profileData.state;
      this.zip = this.profileData.zip;
      this.phone = this.profileData.phone;
      this.emgnphone = this.profileData.emg_phone;
      this.email = this.profileData.email;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  calculateAge(dob) {
    if (dob != null) {
      var dob_entry = dob;
      var split_dob = dob_entry.split("T");
      var date_only = split_dob[0];
      var split_date_only = date_only.split("-");
      var month = split_date_only[1];
      var day = split_date_only[2];
      var year = split_date_only[0];
      var today = new Date();
      var age = today.getFullYear() - year;
      if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
        age--;
      }
      return age;
    }
  }


  onNext() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
    if (this.servicetype == '') {
      this.presentToast('Please select Service Type');
    } else if (this.fname.trim() == '') {
      this.presentToast('Please enter your First Name');
    } else if (namePattern.test(this.fname)) {
      this.presentToast('First Name field should be Only Letters...');
    } else if (this.lname.trim() == '') {
      this.presentToast('Please enter your Last Name');
    } else if (namePattern.test(this.lname)) {
      this.presentToast('Last Name field should be Only Letters...');
    } else if (this.servicetype != 3 && this.dob.trim() == '') {
      this.presentToast('Please choose Date of Birth');
    } else if (this.servicetype != 3 && this.calculateAge(this.dob) < 16) {
      this.presentToast('Age should be greater than or equal to 16 years');
    } else if (this.address.trim() == '') {
      this.presentToast('Please enter Address');
    } else if (this.city.trim() == '') {
      this.presentToast('Please enter City');
    } else if (namePattern.test(this.city)) {
      this.presentToast('City field should be Only Letters...');
    } else if (this.state.trim() == '') {
      this.presentToast('Please enter State');
    } else if (namePattern.test(this.state)) {
      this.presentToast('State field should be Only Letters...');
    } else if (this.zip.trim() == '') {
      this.presentToast('Please enter Zip');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.emgnphone.trim() == '') {
      this.presentToast('Please enter Emergency Phone Number');
    } else if (phonePattern.test(this.emgnphone)) {
      this.presentToast('Emergency Phone field should be Only Numbers...');
    } else if (this.emgnphone.length <= 9) {
      this.presentToast('Emergency Phone! Please enter minimum 10 Numbers');
    } else if (this.email.trim() == '') {
      this.presentToast('Please enter your Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      if (this.userdata == null) {
        if (this.password.trim() == '') {
          this.presentToast('Please enter your Password');
        } else if (this.password.length < 8) {
          this.presentToast('Password must be contain atleast 8 letters');
        } else if (!passPattern.test(this.password)) {
          this.presentToast('Password must be contain atleast one lowercase and one uppercase letter one digit and one special character.');
        } else if (this.cnfpassword.trim() == '') {
          this.presentToast('Please enter confirm password');
        } else if (this.cnfpassword != this.password) {
          this.presentToast('You Password and Confirm password must match');
        } else {
          this.showLoader('Please wait...');
          var body = {
            email: this.email,
          }
          this.authService.postData("check-email", body).then(result => {
            this.data = result;
            if (this.data.status.error_code == 0) {
              this.hideLoader();
              var object = {
                servicetype: this.servicetype,
                fname: this.fname,
                lname: this.lname,
                dob: this.dob,
                address: this.address,
                city: this.city,
                state: this.state,
                zip: this.zip,
                phone: this.phone,
                emgnphone: this.emgnphone,
                email: this.email,
                password: this.password
              }
              this.presentToast('Successfully Proceed..');
              this.storage.set("providerRegsData", object);
              this.storage.set("setp1", object);
              this.navCtrl.navigateForward("/providerregistration2");
            } else {
              this.presentToast("Sorry! Email already Registered.");
              this.hideLoader();
            }
          },
            error => {
              this.hideLoader();
            });
        }
      } else {
        this.showLoader('Please wait...');
        var editObject = {
          servicetype: this.servicetype,
          fname: this.fname,
          lname: this.lname,
          dob: this.dob,
          address: this.address,
          city: this.city,
          state: this.state,
          zip: this.zip,
          phone: this.phone,
          emgnphone: this.emgnphone,
          email: this.email,
        }
        this.presentToast('Successfully Proceed..');
        this.storage.set("providerRegsData", editObject);
        setTimeout(() => {
          this.hideLoader();
          this.navCtrl.navigateForward("/providerregistration2");
        }, 100)
      }
    }
  }


  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
