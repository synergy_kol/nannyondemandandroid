import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Providerregistration3Page } from './providerregistration3.page';

const routes: Routes = [
  {
    path: '',
    component: Providerregistration3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Providerregistration3PageRoutingModule {}
