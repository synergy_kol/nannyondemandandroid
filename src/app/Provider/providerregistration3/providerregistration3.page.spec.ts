import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Providerregistration3Page } from './providerregistration3.page';

describe('Providerregistration3Page', () => {
  let component: Providerregistration3Page;
  let fixture: ComponentFixture<Providerregistration3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Providerregistration3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Providerregistration3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
