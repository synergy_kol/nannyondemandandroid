import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentConnectPageRoutingModule } from './payment-connect-routing.module';

import { PaymentConnectPage } from './payment-connect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentConnectPageRoutingModule
  ],
  declarations: [PaymentConnectPage]
})
export class PaymentConnectPageModule {}
