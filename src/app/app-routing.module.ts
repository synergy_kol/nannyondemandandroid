import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Network } from '@ionic-native/network/ngx';
import { HttpClientModule } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Device } from '@ionic-native/device/ngx';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'forgotpass',
    loadChildren: () => import('./forgotpass/forgotpass.module').then(m => m.ForgotpassPageModule)
  },
  {
    path: 'registration1',
    loadChildren: () => import('./User/registration1/registration1.module').then(m => m.Registration1PageModule)
  },
  {
    path: 'registration2',
    loadChildren: () => import('./User/registration2/registration2.module').then(m => m.Registration2PageModule)
  },
  {
    path: 'congratulation',
    loadChildren: () => import('./User/congratulation/congratulation.module').then(m => m.CongratulationPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./User/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'nannylist',
    loadChildren: () => import('./User/nannylist/nannylist.module').then(m => m.NannylistPageModule)
  },
  {
    path: 'nannydetails',
    loadChildren: () => import('./User/nannydetails/nannydetails.module').then(m => m.NannydetailsPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./User/payment/payment.module').then(m => m.PaymentPageModule)
  },
  {
    path: 'successpayment',
    loadChildren: () => import('./User/successpayment/successpayment.module').then(m => m.SuccesspaymentPageModule)
  },
  {
    path: 'myprofile',
    loadChildren: () => import('./User/myprofile/myprofile.module').then(m => m.MyprofilePageModule)
  },
  {
    path: 'mybookings',
    loadChildren: () => import('./User/mybookings/mybookings.module').then(m => m.MybookingsPageModule)
  },
  {
    path: 'bookingdetails',
    loadChildren: () => import('./User/bookingdetails/bookingdetails.module').then(m => m.BookingdetailsPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./User/help/help.module').then(m => m.HelpPageModule)
  },
  {
    path: 'termsconditions',
    loadChildren: () => import('./User/termsconditions/termsconditions.module').then(m => m.TermsconditionsPageModule)
  },
  {
    path: 'changepassword',
    loadChildren: () => import('./User/changepassword/changepassword.module').then( m => m.ChangepasswordPageModule)
  },
  {
    path: 'wrte-review',
    loadChildren: () => import('./User/wrte-review/wrte-review.module').then( m => m.WrteReviewPageModule)
  },
  {
    path: 'calender-view',
    loadChildren: () => import('./User/calender-view/calender-view.module').then( m => m.CalenderViewPageModule)
  },
  {
    path: 'providerregistration1',
    loadChildren: () => import('./Provider/providerregistration1/providerregistration1.module').then(m => m.Providerregistration1PageModule)
  },
  {
    path: 'providerregistration2',
    loadChildren: () => import('./Provider/providerregistration2/providerregistration2.module').then(m => m.Providerregistration2PageModule)
  },
  {
    path: 'providerregistration3',
    loadChildren: () => import('./Provider/providerregistration3/providerregistration3.module').then(m => m.Providerregistration3PageModule)
  },
  {
    path: 'providercongratulation',
    loadChildren: () => import('./Provider/providercongratulation/providercongratulation.module').then(m => m.ProvidercongratulationPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./Provider/dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'providerprofile',
    loadChildren: () => import('./Provider/providerprofile/providerprofile.module').then(m => m.ProviderprofilePageModule)
  },
  {
    path: 'providerbookings',
    loadChildren: () => import('./Provider/providerbookings/providerbookings.module').then(m => m.ProviderbookingsPageModule)
  },
  {
    path: 'providerbookingdetails',
    loadChildren: () => import('./Provider/providerbookingdetails/providerbookingdetails.module').then(m => m.ProviderbookingdetailsPageModule)
  },
  {
    path: 'earnings',
    loadChildren: () => import('./Provider/earnings/earnings.module').then(m => m.EarningsPageModule)
  },
  {
    path: 'providernotification',
    loadChildren: () => import('./Provider/providernotification/providernotification.module').then(m => m.ProvidernotificationPageModule)
  },
  {
    path: 'providerhelp',
    loadChildren: () => import('./Provider/providerhelp/providerhelp.module').then(m => m.ProviderhelpPageModule)
  },
  {
    path: 'providertermasconditions',
    loadChildren: () => import('./Provider/providertermasconditions/providertermasconditions.module').then(m => m.ProvidertermasconditionsPageModule)
  },
  {
    path: 'providerchangepassword',
    loadChildren: () => import('./Provider/providerchangepassword/providerchangepassword.module').then( m => m.ProviderchangepasswordPageModule)
  },
  {
    path: 'payment-connect',
    loadChildren: () => import('./Provider/payment-connect/payment-connect.module').then( m => m.PaymentConnectPageModule)
  },
  { 
    path: 'filter', 
    loadChildren: () => import('./User/filter/filter.module').then( m => m.FilterPageModule) 
  },
  {
    path: 'schedule',
    loadChildren: () => import('./User/schedule/schedule.module').then( m => m.SchedulePageModule)
  },
];

@NgModule({
  imports: [
    HttpClientModule,
    IonicStorageModule.forRoot(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [
    FileTransfer,
    AuthServiceProvider,
    Network,
    ScreenOrientation,
    Camera,
    FilePath,
    File,
    Calendar,
    PayPal,
    Stripe,
    InAppBrowser,
    Device
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
