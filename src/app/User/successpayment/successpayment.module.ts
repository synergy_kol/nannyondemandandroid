import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccesspaymentPageRoutingModule } from './successpayment-routing.module';

import { SuccesspaymentPage } from './successpayment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccesspaymentPageRoutingModule
  ],
  declarations: [SuccesspaymentPage]
})
export class SuccesspaymentPageModule {}
