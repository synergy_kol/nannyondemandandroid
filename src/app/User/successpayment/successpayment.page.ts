import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-successpayment',
  templateUrl: './successpayment.page.html',
  styleUrls: ['./successpayment.page.scss'],
})
export class SuccesspaymentPage implements OnInit {

  constructor(
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  goBooking() {
    this.navCtrl.navigateRoot('/mybookings');
  }

}
