import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Stripe } from '@ionic-native/stripe/ngx';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { PayPal, PayPalConfiguration, PayPalPayment } from '@ionic-native/paypal/ngx';
import { Storage } from '@ionic/storage';
import { scheduled } from 'rxjs';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  paymentData: any = [{ name: 'Saved Cards' }, { name: 'New Card' }, { name: 'Paypal' }];
  selectedChip: any = 1;
  cardSelectedChip: any;
  cardholdername: any = '';
  cardnumber: any = '';
  expirydate: any = '';
  cvv: any = '';
  remember: boolean = false;
  isDisabled: boolean = false;
  isLoading = false;
  data: any;
  userID: any;
  userName: any;
  paypalData: any; 
  month: any = '';
  year: any = '';
  cardData: any = [];
  mycardData: any;
  userdata: any [];
  order_final_total: any;

  paydata: any = [];

  currentDate: any = (new Date()).getFullYear();
  maxDate: any = (new Date()).getFullYear() + 100;

  constructor(
    private stripe: Stripe,
    private payPal: PayPal,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.storage.get('payDetails').then((val) => {
      this.paydata = val;
      console.log('Pay:', this.paydata);
    });
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = this.data.user_id;
      var body = {
        user_id: this.data.user_id,
        scheduled_list: this.paydata
      };
      this.authService.postData("get-nanny-charge", body).then(result => {
        this.data = result;
        console.log("GetCharge:", this.data);
        this.order_final_total = this.data.result.data.charge;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
          console.log(error);
        });
    })    
  }

  getMonth(mydate) {
    var dateTimeArr = mydate.split('T');
    var dateArr = dateTimeArr[0];
    var newDateArr = String(dateArr).split('-');
    return newDateArr[1].toString();
  }

  getYear(mydate) {
    var dateTimeArr = mydate.split('T');
    var dateArr = dateTimeArr[0];
    var newDateArr = String(dateArr).split('-');
    return newDateArr[0].toString;
  }

  cardSelected(data, ind) {
    this.mycardData = data;
    this.cardSelectedChip = ind;
    console.log(this.mycardData);
  }

  onPay() {
    //this.navCtrl.navigateRoot('/successpayment');
    if (this.selectedChip == 0) {
      if (this.cardSelectedChip == null) {
        this.presentToast('Please select Card');
      } else {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((result) => {
          this.data = result;
          this.userID = this.data.user_id;
          this.userName = this.data.fname + " " + this.data.lname;
          console.log(this.mycardData.account_name);
          this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
          let card = {
            number: this.mycardData.card_no,
            expMonth: this.mycardData.exp_month,
            expYear: this.mycardData.exp_year,
            cvc: this.mycardData.card_cvc
          };
          this.stripe.createCardToken(card)
            .then(token => {
              console.log("token: ", token.id);
              var body = {
                user_id: this.userID,
                //nany_id: this.nanny_id,
                booking_amount: this.order_final_total,
                transaction_type: 'stripe',
                token_id: token.id
              };
              this.authService.postData("store-booking", body).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                  this.hideLoader();
                  this.presentToast(this.data.status.message);
                  this.navCtrl.navigateForward('successpayment');
                  console.log(this.data);
                } else {
                  console.log(this.data.status.message);
                }
              },
                error => {
                  this.hideLoader();
                  console.log(error);
                });
            })
            .catch(error => {
              console.error("error: ", error);
              this.hideLoader();
            });
        });
      }
    } else if (this.selectedChip == 1) {
      if (this.remember == true) {
        var dateTimeArr = this.expirydate.split('T');
        var dateArr = dateTimeArr[0];
        var newDateArr = String(dateArr).split('-');
        this.month = newDateArr[1];
        this.year = newDateArr[0];
        if (this.cardholdername.trim() == '') {
          this.presentToast('Please enter your Full Name');
        } else if (this.cardnumber.trim() == '') {
          this.presentToast('Please enter Card Number');
        } else if (this.expirydate.trim() == '') {
          this.presentToast('Please enter Expiry Date');
        } else if (this.cvv.trim() == '') {
          this.presentToast('Please enter CVC number');
        } else {
          this.showLoader('Please wait...');
          this.storage.get('userDetails').then((result) => {
            this.data = result;
            this.userID = this.data.user_id;
            this.userName = this.data.fname + " " + this.data.lname;
            var body = {
              user_id: this.userID,
              card_no: this.cardnumber,
              exp_month: this.month,
              exp_year: this.year,
              cvc: this.cvv,
              account_name: this.cardholdername
            }
            this.authService.postData("save-card", body).then(result => {
              this.data = result;
              console.log("save-card: ", this.data);
              if (this.data.status.error_code == 0) {
                this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
                let card = {
                  number: this.cardnumber,
                  expMonth: this.month,
                  expYear: this.year,
                  cvc: this.cvv
                }
                this.stripe.createCardToken(card)
                  .then(token => {
                    console.log("token: ", token.id);
                    var body = {
                      user_id: this.userID,
                      //nany_id: this.nanny_id,
                      booking_amount: this.order_final_total,
                      transaction_type: 'stripe',
                      token_id: token.id
                    };
                    this.authService.postData("store-booking", body).then(result => {
                      this.data = result;
                      if (this.data.status.error_code == 0) {
                        this.hideLoader();
                        this.presentToast(this.data.status.message);
                        this.navCtrl.navigateForward('successpayment');
                        console.log(this.data);
                      } else {
                        console.log(this.data.status.message);
                      }
                    },
                      error => {
                        this.hideLoader();
                        console.log(error);
                      });
                  })
                  .catch(error => {
                    console.error("error: ", error);
                    this.hideLoader();
                  });
              } else {
                console.log(this.data.status.message);
                this.hideLoader();
              }
            },
              error => {
                this.hideLoader();
                console.log(error);
              });
          });
        }
      } else {
        var dateTimeArr = this.expirydate.split('T');
        var dateArr = dateTimeArr[0];
        var newDateArr = String(dateArr).split('-');
        this.month = newDateArr[1];
        this.year = newDateArr[0];
        if (this.cardholdername.trim() == '') {
          this.presentToast('Please enter your Full Name');
        } else if (this.cardnumber.trim() == '') {
          this.presentToast('Please enter Card Number');
        } else if (this.expirydate.trim() == '') {
          this.presentToast('Please enter Expiry Date');
        } else if (this.cvv.trim() == '') {
          this.presentToast('Please enter CVC number');
        } else {
          console.log(this.expirydate);
          this.showLoader('Please wait...');
          this.storage.get('userDetails').then((result) => {
            this.data = result;
            this.userID = this.data.user_id;
            this.userName = this.data.fname + " " + this.data.lname;
            this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
            let card = {
              number: this.cardnumber,
              expMonth: this.month,
              expYear: this.year,
              cvc: this.cvv
            }
            this.stripe.createCardToken(card)
              .then(token => {
                console.log("token: ", token.id);
                var body = {
                  user_id: this.userID,
                  //nany_id: this.nanny_id,
                  booking_amount: this.order_final_total,
                  transaction_type: 'stripe',
                  token_id: token.id
                };
                this.authService.postData("store-booking", body).then(result => {
                  this.data = result;
                  if (this.data.status.error_code == 0) {
                    this.hideLoader();
                    this.presentToast(this.data.status.message);
                    this.navCtrl.navigateForward('successpayment');
                    console.log(this.data);
                  } else {
                    console.log(this.data.status.message);
                  }
                },
                  error => {
                    this.hideLoader();
                    console.log(error);
                  });
              })
              .catch(error => {
                console.error("error: ", error);
                this.hideLoader();
              });
          });
        }
      }
    } 
    // else {
    //   this.storage.get('userDetails').then((result) => {
    //     this.data = result;
    //     this.userID = this.data.user_id;
    //     this.userName = this.data.fname + " " + this.data.lname;
    //     this.payPal.init({
    //       PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
    //       PayPalEnvironmentSandbox: 'AbzaJjjdSZ7ub9OZwF_8Ns40vJBy0WCzHeiYzTxA6BmmugGLzgNL_NbBvmaixcXQGc7Bc_O-7ONLMHoJ'
    //     }).then(() => {
    //       this.payPal.prepareToRender('PayPalEnvironmentProduction', new PayPalConfiguration({
    //       })).then(() => {
    //         let payment = new PayPalPayment((this.order_final_total).toString(), 'USD', 'Description', 'sale');
    //         this.payPal.renderSinglePaymentUI(payment).then((response) => {
    //           console.log(response);
    //           this.paypalData = response;
    //           var body = {
    //             user_id: this.userID,
    //             //nany_id: this.nanny_id,
    //             booking_amount: this.order_final_total,
    //             transaction_type: 'paypal',
    //             transaction_id: this.paypalData.response.id
    //           };
    //           this.authService.postData("store-booking", body).then(result => {
    //             this.data = result;
    //             if (this.data.status.error_code == 0) {
    //               this.hideLoader();
    //               this.presentToast(this.data.status.message);
    //               this.navCtrl.navigateForward('successpayment');
    //               console.log(this.data);
    //             } else {
    //               console.log(this.data.status.message);
    //             }
    //           },
    //             error => {
    //               this.hideLoader();
    //               console.log(error);
    //             });
    //         }, () => {
    //           this.hideLoader();
    //         });
    //       }, () => {
    //         this.hideLoader();
    //       });
    //     }, () => {
    //       this.hideLoader();
    //     });
    //   });
    // }

  }

  onFilterPayment(id) {
    this.selectedChip = id;
    if (id == 0) {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = this.data.user_id;
        var body = {
          user_id: this.userID
        }
        this.authService.postData("get-cards", body).then(result => {
          this.data = result;
          this.cardData = this.data.result.data;
          console.log("get-cards: ", this.data);
          this.hideLoader();
        },
          error => {
            this.hideLoader();
            console.log(error);
          });
      });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
