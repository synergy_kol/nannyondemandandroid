import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NannydetailsPageRoutingModule } from './nannydetails-routing.module';
import { NannydetailsPage } from './nannydetails.page';
import { StarRatingModule } from 'ionic5-star-rating';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NannydetailsPageRoutingModule,
    StarRatingModule,
    NgxIonicImageViewerModule
  ],
  declarations: [NannydetailsPage]
})
export class NannydetailsPageModule {}
