import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NannydetailsPage } from './nannydetails.page';

const routes: Routes = [
  {
    path: '',
    component: NannydetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NannydetailsPageRoutingModule {}
