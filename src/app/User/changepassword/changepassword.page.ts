import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {

  isLoading = false;
  isDisabled: boolean = false;
  password: any;
  new_password: any;
  confirm_password: any;
  userdata: any = [];
  data: any;

  constructor(
    private storage: Storage,
    public authService: AuthServiceProvider,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public navCtrl: NavController
  ) { }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onUpdate();
    }
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.loginToken();
  }

  ngOnInit() {
  }
  

  onUpdate(){
    const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
    if (this.password == null) {
      this.presentToast('Please enter your Old password');
    } else if (this.new_password == null) {
      this.presentToast('Please enter New password');
    } else if (this.new_password.length < 8) {
      this.presentToast('Please enter Minimum 8 Characters.');
    } else if (!passPattern.test(this.new_password)){
      this.presentToast('Password must be contain atleast one lowercase and one uppercase letter one digit and one special character.');
    } else if (this.confirm_password == null) {
      this.presentToast('Please enter confirm New password');
    } else if (this.confirm_password != this.new_password) {
      this.presentToast('New Password and Confirm new password must match');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.userdata = val;
        console.log("profile: ", val);
        var body = { 
          u_id: this.userdata.user_id, 
          oldPassword: this.password, 
          newPassword: this.new_password
        };
        this.authService.postData("change-password", body).then(result => {
          this.data = result;
          console.log("changePassword: ", this.data);
          if (this.data.status.error_code == 0) {
            this.presentToast("Password has been Changed Successfully.");
            this.navCtrl.pop();
          } else {
            this.presentToast(this.data.status.message);
          }
          this.hideLoader();
        },
          error => {
            console.log(error);
            this.hideLoader();
          });
      });

    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
