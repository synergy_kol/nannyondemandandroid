import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {

  pop = false;
  data: any = [];
  userdata: any = [];
  isDisabled: boolean = false;
  isLoading = false;
  minutes: any = 29;
  seconds: any = 60;

  scheduleData: any = [];
  payon: '';
  today = new Date().toISOString();
  date: any = "";
  timeslot: any = "";
  workinghour: any = "";

  scheduleSelectedData: any = [];

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onChecked(ev, data, indx){
    if(ev.target.checked){
      this.scheduleSelectedData.push(data);
    }else {
      this.scheduleSelectedData.splice(indx, 1); 
    }    
    console.log(this.scheduleSelectedData);
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.loginToken();
    this.findSchedule();
  }

  findSchedule() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      var scghrdulebody = {
        user_id: this.data.user_id
      };
      this.authService.postData("get-booked-schedule", scghrdulebody).then(result => {
        this.data = result;
        console.log("Schedule:", this.data);
        if (this.data.status.error_code == 0) {
          this.scheduleData = this.data.result.data;
          this.payon = this.data.result.is_accepted;
        } else {
          this.presentToast(this.data.status.message);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    })
  }

  onBook() {
    this.showLoader('Loading...');
    this.storage.set("payDetails", this.scheduleSelectedData);
    setTimeout(() => {
      this.scheduleSelectedData = [];      
      this.navCtrl.navigateForward("/payment");
      this.hideLoader();
    }, 3000)    
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
