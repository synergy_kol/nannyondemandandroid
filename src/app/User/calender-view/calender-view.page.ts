import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-calender-view',
  templateUrl: './calender-view.page.html',
  styleUrls: ['./calender-view.page.scss'],
})

export class CalenderViewPage implements OnInit {

  pop = false;
  todayDate = new Date().toISOString();
  date: any = "";
  timeslot: any = "";
  workinghour: any = "";

  lockSwipes: boolean = true;
  userdata: any = [];
  nanny_id: any = '';
  rate: any;
  rate_type: any;
  availabilities: any = [];
  selectedDay: any = [];
  isLoading = false;
  data: any;
  isDisabled: boolean = false;
  eventSource;
  viewTitle;
  isToday: boolean;
  currentDate: any;
  maxDate: any;
  customActionSheetOptions: any = {

  };
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    dateFormatter: {
      formatMonthViewDay: function (date: Date) {
        return date.getDate().toString();
      },
      formatMonthViewDayHeader: function (date: Date) {
        return 'MonMH';
      },
      formatMonthViewTitle: function (date: Date) {
        return 'testMT';
      },
      formatWeekViewDayHeader: function (date: Date) {
        return 'MonWH';
      },
      formatWeekViewTitle: function (date: Date) {
        return 'testWT';
      },
      formatWeekViewHourColumn: function (date: Date) {
        return 'testWH';
      },
      formatDayViewHourColumn: function (date: Date) {
        return 'testDH';
      },
      formatDayViewTitle: function (date: Date) {
        return 'testDT';
      }
    }
  };

  scheduleData: any = [];

  constructor(
    public storage: Storage,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    public authService: AuthServiceProvider,
    public toastController: ToastController,
    public loadingController: LoadingController
  ) { }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.loginToken();
    this.findSchedule();
  }

  findSchedule() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      var scghrdulebody = {
        user_id: this.data.user_id
      };
      this.authService.postData("get-booked-schedule", scghrdulebody).then(result => {
        this.data = result;
        console.log("Schedule:", this.data);
        if (this.data.status.error_code == 0) {
          this.scheduleData = this.data.result.data;
        } else {
          this.presentToast(this.data.status.message);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    })
  }

  ngOnInit() {
    var my_date = new Date().toISOString();
    this.currentDate = my_date.split('T')[0];
    console.log('currentDate: ', this.currentDate);
    this.maxDate = (new Date()).getFullYear() + 100;
    console.log('maxDate: ', this.maxDate);
    this.activatedRoute.queryParams.subscribe((res) => {
      this.nanny_id = res.nanny_id;
      this.rate_type = res.rate_type;
      this.rate = res.rate;
      this.availabilities = JSON.parse(res.availabilities);
    });
    for (var i = 0; i < this.availabilities.length; i++) {
      this.getMyEventDay(this.availabilities[i].day, this.availabilities[i].time);
    }
  }
  onView() {
    this.navCtrl.navigateForward("/schedule");
  }
  onCheck() {
    this.pop = true;
  }
  closepop() {
    this.pop = false;
  }
  onSave() {
    if (this.date == "") {
      this.presentToast('Please Select Date');
    } else if (this.timeslot == "") {
      this.presentToast('Please Select Time Slot');
    } else if (this.workinghour.trim() == "") {
      this.presentToast('Please Enter Working Hour');
    } else if (this.workinghour >= 24) {
      this.presentToast('Working Hour should be less than 24hr');
    } else {
      this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
          var body = {
            user_id: val.user_id,
            nany_id: this.nanny_id,
            date: moment(this.date).format('DD-MM-YYYY'),
            time: this.timeslot,
            hour: this.workinghour,
            payment: this.rate * this.workinghour
          };
          this.authService.postData("check-availability", body).then(result => {
            this.data = result;
            console.log("check-availability: ", this.data);
            if (this.data.status.error_code == 0) {
              this.presentToast("Successfully! Please Wait for Nanny's Approval.<br/>Sent Your Booking Request to Nanny.");
              this.pop = false;
              this.date = '';
              this.timeslot = '';
              this.workinghour = '';
              this.hideLoader();
              this.navCtrl.navigateForward("/schedule");
            } else {
              this.presentToast(this.data.status.message);
              this.hideLoader();
            }
          },
            error => {
              this.hideLoader();
            });
        });
    }
  }

  markDisabled = (date: Date) => {
    var current = new Date();
    return date < current;
  };

  getMyEventDay(day, time) {
    var selectedDay = moment()
      .startOf('month')
      .day(day);
    if (selectedDay.date() > 7) selectedDay.add(7, 'd');
    var month = selectedDay.month();
    while (month === selectedDay.month()) {
      console.log(selectedDay, ' : ', selectedDay.toString());
      this.selectedDay.push({ day: selectedDay.toString(), time: time });
      selectedDay.add(7, 'd');
    }
    console.log('selectedDay:', this.selectedDay);
    this.eventSource = this.createRandomEvents();
  }

  loadEvents() {
    this.eventSource = this.createRandomEvents();
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    alert("Hello");
  }

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    if ((ev.events !== undefined && ev.events.length !== 0) == true) {
      console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
        (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
    }
  }

  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }

  formatColor(timeslot) {
    if (timeslot == 'Morning') {
      return 'green'
    } else if (timeslot == 'Night') {
      return 'red'
    } else {
      return 'blue'
    }
  }

  createRandomEvents() {
    var events = [];
    for (var i = 0; i < this.selectedDay.length; i++) {
      events.push({
        title: this.selectedDay[i].time,
        startTime: new Date(new Date(this.selectedDay[i].day).getFullYear(), new Date(this.selectedDay[i].day).getMonth(), new Date(this.selectedDay[i].day).getDate()),
        endTime: new Date(new Date(this.selectedDay[i].day).getFullYear(), new Date(this.selectedDay[i].day).getMonth(), new Date(this.selectedDay[i].day).getDate()),
        allDay: false,
        timeslot: this.selectedDay[i].time
      });
    }
    console.log('events: ', events);
    return events;
  }

  onRangeChanged(ev) {
    console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }

  formatDate(mydate) {
    var dateTimeArr = mydate.split('T');
    var dateArr = dateTimeArr[0];
    return dateArr;
  }

  formatTime(mydate) {
    var dateTimeArr = mydate.split('T');
    var timeArr = dateTimeArr[1];
    var newtimeArr = String(timeArr).split(':');
    var hrStr;
    var ampmStr;
    if (Number(newtimeArr[0]) > 12) {
      hrStr = Number(newtimeArr[0]) - 12;
      ampmStr = 'PM';
    } else {
      hrStr = newtimeArr[0];
      ampmStr = 'AM';
    }
    return hrStr + ':' + newtimeArr[1] + ' ' + ampmStr;
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 5000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
