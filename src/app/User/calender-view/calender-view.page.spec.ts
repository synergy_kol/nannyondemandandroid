import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalenderViewPage } from './calender-view.page';

describe('CalenderViewPage', () => {
  let component: CalenderViewPage;
  let fixture: ComponentFixture<CalenderViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalenderViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
