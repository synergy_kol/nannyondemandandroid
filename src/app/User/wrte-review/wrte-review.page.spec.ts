import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WrteReviewPage } from './wrte-review.page';

describe('WrteReviewPage', () => {
  let component: WrteReviewPage;
  let fixture: ComponentFixture<WrteReviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrteReviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WrteReviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
